package com.example.cuncis.uangkas;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.example.cuncis.uangkas.database.DatabaseHelper;

public class AddActivity extends AppCompatActivity {

    RadioGroup radioStatus;
    RadioButton radioMasuk, radioKeluar;
    EditText editJumlah, editKeterangan;
    Button btnSimpan;
    RippleView ripSimpan;

    String status;

    DatabaseHelper dbHelper;
    Cursor cursor;      //menentukan kolom keberapa

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        status = "";
        dbHelper = new DatabaseHelper(this);

        radioStatus     = findViewById(R.id.radio_status);
        radioMasuk      = findViewById(R.id.radio_masuk);
        radioKeluar     = findViewById(R.id.radio_keluar);
        editJumlah      = findViewById(R.id.edit_jumlah);
        editKeterangan  = findViewById(R.id.edit_keterangan);
        btnSimpan       = findViewById(R.id.btn_simpan);
        ripSimpan       = findViewById(R.id.rip_simpan);

        radioStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_masuk:
                        status = "MASUK";
                        break;
                    case R.id.radio_keluar:
                        status = "KELUAR";
                        break;
                }
            }
        });

        ripSimpan.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                if (status.equals("") ||  editJumlah.getText().toString().trim().equals("")
                        || editKeterangan.getText().toString().trim().equals("")) {
                    Toast.makeText(AddActivity.this, "Isi data dengan benar :(", Toast.LENGTH_SHORT).show();
                }else if (status.equals("")) {
                    Toast.makeText(AddActivity.this, "Status harus diisi", Toast.LENGTH_SHORT).show();
                } else if (editJumlah.getText().toString().trim().equals("")) {
                    Toast.makeText(AddActivity.this, "Jumlah harus diisi", Toast.LENGTH_SHORT).show();
                } else if (editKeterangan.getText().toString().trim().equals("")) {
                    Toast.makeText(AddActivity.this, "Keterangan harus diisi", Toast.LENGTH_SHORT).show();
                } else {
                    simpanData();
                }
            }
        });

        getSupportActionBar().setTitle("Tambah");   //tambah judulnya
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  //tambahin panah *back

    }

    private void simpanData() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("INSERT INTO " + DatabaseHelper.DB_TABLE + "(status, jumlah, keterangan) VALUES (" +
                "'" + status + "', '" + editJumlah.getText().toString().trim() + "', " +
                "'" + editKeterangan.getText().toString().trim() + "')");

        Toast.makeText(this, "Transaksi berhasil disimpan :)", Toast.LENGTH_SHORT).show();
        finish();       //activity akan menutup otomatis dan kembali ke MainActivity
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();   //untuk aksinya (menutup halaman yg kita buka)
        return true;
    }
}
