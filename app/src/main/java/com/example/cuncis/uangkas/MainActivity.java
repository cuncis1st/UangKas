package com.example.cuncis.uangkas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.example.cuncis.uangkas.database.DatabaseHelper;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    TextView textMasuk, textKeluar, textTotal;
    ListView listKas;
    SwipeRefreshLayout swipeRefresh;

    DatabaseHelper dbHelper;
    Cursor cursor;      //menentukan kolom keberapa

    ArrayList<HashMap<String, String>> arusKas;

    String queryKas, queryTotal;
    public static boolean filter;

    public static String transaksiId, tgl_dari, tgl_ke;
    public static TextView textFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeRefresh = findViewById(R.id.swipe_refresh);
        textMasuk = findViewById(R.id.text_masuk);
        textKeluar = findViewById(R.id.text_keluar);
        textTotal = findViewById(R.id.text_saldo);
        listKas = findViewById(R.id.listView);
        textFilter = findViewById(R.id.text_filter);
        arusKas = new ArrayList<>();

        FloatingActionButton floatingActionButton = findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Fab button clicked", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, AddActivity.class));
            }
        });

        dbHelper = new DatabaseHelper(this);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                queryKas = "SELECT *, strftime('%d/%m/%Y', tanggal) AS tgl FROM " + DatabaseHelper.DB_TABLE + " ORDER BY transaksi_id DESC";
                queryTotal = "SELECT SUM (jumlah) AS total," +
                        "(SELECT SUM (jumlah) FROM transaksi WHERE status='MASUK') AS masuk," +
                        "(SELECT SUM (jumlah) FROM transaksi WHERE status='KELUAR') AS keluar FROM transaksi";
                kasAdapter();
            }
        });
    }

    private void listMenu() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setTitle("pilih: ");
        dialog.setContentView(R.layout.list_menu);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        RippleView ripHapus = dialog.findViewById(R.id.rip_hapus);
        RippleView ripEdit = dialog.findViewById(R.id.rip_edit);

        ripHapus.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                dialog.dismiss();
                hapus();
            }
        });

        ripEdit.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                dialog.dismiss();
                edit();
            }
        });

        dialog.show();
    }

    private void edit() {
        startActivity(new Intent(MainActivity.this, EditActivity.class));
    }

    private void hapus() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Apa kamu yakin?");
        builder.setPositiveButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("DELETE FROM " + DatabaseHelper.DB_TABLE + " WHERE transaksi_id = '" + transaksiId + "'");

                kasAdapter();
                Toast.makeText(MainActivity.this, "Data berhasil dihapus", Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }

    @Override
    protected void onResume() {     //ketika activity masuk -> terus diapain
        super.onResume();

        queryKas = "SELECT *, strftime('%d/%m/%Y', tanggal) AS tgl FROM " + DatabaseHelper.DB_TABLE + " ORDER BY transaksi_id DESC";
        queryTotal = "SELECT SUM (jumlah) AS total," +
                "(SELECT SUM (jumlah) FROM transaksi WHERE status='MASUK') AS masuk," +
                "(SELECT SUM (jumlah) FROM transaksi WHERE status='KELUAR') AS keluar FROM transaksi";

        if (filter) {
            queryKas = "SELECT *, strftime('%d/%m/%Y', tanggal) AS tgl FROM " + DatabaseHelper.DB_TABLE + " " +
                    "WHERE (tanggal >= '" + tgl_dari + "') AND (tanggal <= '" + tgl_ke + "') ORDER BY transaksi_id DESC";
            queryTotal = "SELECT SUM (jumlah) AS total," +
                    "(SELECT SUM (jumlah) FROM transaksi WHERE status='MASUK' AND (tanggal >= '" + tgl_dari + "') AND (tanggal <= '" + tgl_ke + "')) AS masuk," +
                    "(SELECT SUM (jumlah) FROM transaksi WHERE status='KELUAR' AND (tanggal >= '" + tgl_dari + "') AND (tanggal <= '" + tgl_ke + "')) AS keluar FROM transaksi " +
                    "WHERE (tanggal >= '" + tgl_dari + "') AND (tanggal <= '" + tgl_ke + "')";
        }

        kasAdapter();
    }

    private void kasTotal() {
        NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery(queryTotal,
                null);
        cursor.moveToFirst();

        textMasuk.setText(rupiah.format(cursor.getDouble(1)));
        textKeluar.setText(rupiah.format(cursor.getDouble(2)));
        textTotal.setText(rupiah.format(cursor.getDouble(1) - cursor.getDouble(2)));

        swipeRefresh.setRefreshing(false);  //untuk menghilangkan refresh
        if (!filter) {
            textFilter.setVisibility(View.GONE);
        }

        filter = false;
    }

    private void kasAdapter() {
        arusKas.clear();       //dikosongi dulu agar tidak menumpuk
        listKas.setAdapter(null);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery(queryKas, null);
        //adanya tgl, maka terhitung sebagai index baru
        cursor.moveToFirst();   //mulai dari awal

        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);

            HashMap<String, String> map = new HashMap<>();
            map.put("transaksi_id", cursor.getString(0));
            map.put("status", cursor.getString(1));
            map.put("jumlah", cursor.getString(2));
            map.put("keterangan", cursor.getString(3));
//            map.put("tanggal", cursor.getString(4));
            map.put("tanggal", cursor.getString(5));

            arusKas.add(map);
        }

        SimpleAdapter adapter = new SimpleAdapter(this,
                arusKas,
                R.layout.list_kas,
                new String[]{"transaksi_id", "status", "jumlah", "keterangan", "tanggal"},
                new int[]{R.id.text_transaksiId, R.id.text_status, R.id.text_jumlah, R.id.text_keterangan, R.id.text_tanggal});

        listKas.setAdapter(adapter);
        listKas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                transaksiId = ((TextView) view.findViewById(R.id.text_transaksiId)).getText().toString();
                Log.i(TAG, "onItemLongClick: transaksiId: " + transaksiId);

                listMenu();
            }
        });
//        listKas.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                transaksiId = ((TextView) view.findViewById(R.id.text_transaksiId)).getText().toString();
//                Log.i(TAG, "onItemLongClick: transaksiId: " + transaksiId);
//
//                listMenu();
//
//                return true;
//            }
//        });

        kasTotal();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.filter_item) {
            startActivity(new Intent(MainActivity.this, FilterActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

















