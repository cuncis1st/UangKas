package com.example.cuncis.uangkas;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Toast;

import com.andexert.library.RippleView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

public class FilterActivity extends AppCompatActivity {
    private static final String TAG = "FilterActivity";

    EditText editDari, editKe;
    RippleView rip_filter;

    DatePickerDialog datePickerDialog;

    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int day = calendar.get(Calendar.DAY_OF_MONTH);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        editDari = findViewById(R.id.edit_dari);
        editKe = findViewById(R.id.edit_ke);
        rip_filter = findViewById(R.id.rip_filter);

        editDari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = new DatePickerDialog(FilterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        NumberFormat numberFormat = new DecimalFormat("00");

                        MainActivity.tgl_dari = year + "-" + numberFormat.format(month + 1) + "-" + numberFormat.format(dayOfMonth);
                        Log.i(TAG, "onDateSet: tanggal dari: " + MainActivity.tgl_dari);
                        //data yg akan diambil
                        editDari.setText(numberFormat.format(dayOfMonth) + "/" + numberFormat.format(month+1)
                                + "/" + year);
                    }
                }, year, month, day);   //data yang ingin dimunculkan
                datePickerDialog.show();
            }
        });

        editKe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = new DatePickerDialog(FilterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        NumberFormat numberFormat = new DecimalFormat("00");

                        MainActivity.tgl_ke = year + "-" + numberFormat.format(month + 1) + "-" + numberFormat.format(dayOfMonth);
                        Log.i(TAG, "onDateSet: tanggal ke: " + MainActivity.tgl_ke);
                        //data yg akan diambil
                        editKe.setText(numberFormat.format(dayOfMonth) + "/" + numberFormat.format(month+1)
                                + "/" + year);
                    }
                }, year, month, day);   //data yang ingin dimunculkan
                datePickerDialog.show();
            }
        });

        rip_filter.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                if (editDari.getText().toString().trim().equals("") || editKe.getText().toString().trim().equals("")) {
                    Toast.makeText(FilterActivity.this, "Isi dengan benar :)", Toast.LENGTH_SHORT).show();
                } else {
                    MainActivity.filter = true;
                    MainActivity.textFilter.setText(editDari.getText().toString() + " - " + editKe.getText().toString());
                    MainActivity.textFilter.setVisibility(View.VISIBLE);

                    finish();
                }
            }
        });

        getSupportActionBar().setTitle("Filter");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}















